//CRUD Operations
/*
-CRUD is an acronym for: Create, Red, Update, and Delete
- Create: The create function allows users to create a new record in the database
- Read: The read function is similar to search function. It allows uders to search and retrieve specific records.
- Update: The update function is used to modify existing records that are on our database.
- Delete: The delete function allows users to remove records from a database that is no longer needed.
*/

//CREATE: INSERT Documents
/*
- The mongo sheel uses Javascript for its syntax
- MongoDB deals with objects as it's structure for documents.
- We can create documents by providing objects into our methods.
- Javascript syntax:{object
	- object.object.method({object})
*/

//INSERT ONE
/*
- Syntax:
	-db.collectionName.insertOne({object});
*/

db.users.insert({
	firstName: "Jane",
	lastName: "Done",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	}
})

//INSERT MANY
/*
- Syntax:
	- db.collectionName.insertMany([ {Object A}, {Object B} ])
*/
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		course: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"]
	}
]);

//READ: FIND/RETRIEVE Documents
/*
- The documents will be returned based on their order of storage in  the collection
*/

//FIND ALL DOCUMENTS
/*
- Syntax:
	- db.collectionName.find();
*/
db.users.find();

//FIND USING SINGLE PARAMETER
/*
- Syntax:
	- db.collectionName.find({ field: value })
*/
db.users.find({ firstName: "Stephen"});

//FIND USING MULTIPLE PARAMETERS
/*
- Syntax:
	- db.collectionName.find({ fieldA: valueA, fieldB: valueB })
*/
db.users.find({ lastName: "Armstrong", age: 82 })

// FIND + PRETTY METHOD
/*
- The "pretty" method allows us to b e able to view the documents returned by our terminal in a "prettier" format.
- Syntax:
	- db.collectionName.find({ field: value }).pretty();
*/
db.users.find({ lastName: "Armstrong", age: 82 }).pretty();
db.users.find().pretty();

// UPDATE: EDIT a document

// UPDATE ONE: Updating a single document
/*
- Syntax:
	- db.collectionName.updateOne( {criteria}, {$set:{field:value}});
*/
// For our example, let us create a document that we will then update
// 1. Insert initial document
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: []
	department: "none"
});

//2. Update the document
//MINI ACTIVITY:

db.users.updateOne(
	{ firstName: "Test" },
	{ 
		$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "87654321",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
		status: "active"
			}	
	}
);

db.users.find({ firstName: "Bill" }).pretty();

// UPDATE MANY: Updating multiple documents
/*
- Syntax:
	- db.collectionName:updateMany({criteria}, {$set {field: value}} )
*/
db.users.updateMany(
	{ department: "none"},
	{
		$set: { department: "HR"}
	}
);
db.users.find().pretty();
db.users.find({ department: "HR"}).pretty();

//REPLACE ONE
/*
- Replace one replaces the whole document
- If updateOne updates specific fields, replaceOne replaces the whole document
- If updateOne updates parts, replaceOne replaces the whole document
*/

db.users.insertOne({
	firstName: "John",
	lastName: "Dalisay")
	age: 22,
});

db.users.updateOne(
    { firstName: "John" },
    {
        $set: { lastName: "Dela Cruz" }
    }
);

db.users.find({ firstName: "John" });

db.users.replaceOne(
	{ firstName: "Bill" },
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65
		contact: {
			phone: "12345678",
			email: "bill@rocketmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
	}
);

// DELETE: DELETING Documents
// For our example, let us create a document that we will delete
/*
	- It is good to practice soft deletion or archiving of our documents instead of deleting
*/

db.users.insert({
	firstName: "test"
});

// DELETE ONE: Deleting a single document
/*
- Syntax:
	- db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({
	firstName: "test"
});
db.users.find({ firstName: "test" }).pretty();

//DELETE MANY: Delete many documents
/*
- Syntax:
	- db.collectionName.deleteMany({criteria})
*/
db.users.deleteMany({
	firstName: "Bill"
});
db.users.find({ firstName: "Bill" }).pretty();

//INSERT 2 BILL
/*
db.users.insertMany([
    {
        firstName: "Bill",
        lastName: "Magalang",
        age: 12
    },
    {
        firstName: "Bill",
        lastName: "Batumbakal",
        age: 13
    }
]);
db.users.find({ firstName: "Bill" }) //db.users.find({ isActive: false})
*/

//DELETE ALL: Delete all documents
/*
- Syntax:
	db.collectionName.deleteMany({})
*/

// ADVANCES QUERIES
/*
- Retrieving data with complex data structures is also a good skill for any developer to have
- Real world examples of data can be as complex as having two or more layers of nested objects
- Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
*/

//Query an embedded document
// - An embedded document are those types of documents that contain a document inside a document.
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
}).pretty();

//Query on nested field
db.users.find({
	"contact.email": "janedoe@gmail.com"
}).pretty();

//Query an Array with Exact Elements
db.users.find({courses: ["CSS", "Javascript", "Python"]}).pretty();

// Querying an Array without regard to order
db.users.find({courses: {$all: ["React", "Python" ]}}).pretty()

// Querying an Embedded Array
db.users.insert({
	namearr: [
	{
		nameA: "Juan"

	},
	{
		nameB: "Tamad"
	}
	]
});
db.users.find({
	namearr:
	{
		nameA: "Juan"
	}
}).pretty();